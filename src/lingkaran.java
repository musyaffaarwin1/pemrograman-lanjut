public class lingkaran {
    public double jari;
    private double phi = 3.14;
    private double luas;
    private double keliling;
    private double diameter;

    public double getLuas() {
        luas = phi*jari;
        return luas;
    }

    public double getDiameter() {
        diameter=jari*2;
        return diameter;
    }

    public double getKeliling() {
        keliling = phi*jari*2;
        return keliling;
    }
}
