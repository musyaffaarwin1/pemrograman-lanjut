public class MainObjek1 {
    public static void main(String[] args) {

        // Membuat Objek//
        // Lingkaran //
        lingkaran kepala = new lingkaran();
        lingkaran jaritangankanan = new lingkaran();
        lingkaran jaritangankiri = new lingkaran();
        lingkaran jarikakikiri = new lingkaran();
        lingkaran jarikakikanan = new lingkaran();

        // Persegipanjang //
        persegi badan = new persegi();
        persegi tangankananpersegi = new persegi();
        persegi tangankiripersegi = new persegi();
        persegi kakikanan = new persegi();
        persegi kakikiri = new persegi();

        // Data dari Objek //

        kepala.jari = 12; jaritangankanan.jari = 3; jaritangankiri.jari = 3;
        jarikakikanan.jari = 4;jarikakikiri.jari = 4; tangankananpersegi.panjang = 25;
        tangankiripersegi.panjang = 25;tangankananpersegi.lebar = 6; tangankiripersegi.lebar = 6;
        kakikiri.panjang = 50; kakikanan.panjang = 50; kakikiri.lebar = 10;
        kakikanan.lebar = 10; badan.panjang = 25; badan.lebar = 18;

        //Menghitung luas dan tinggi badan//

        double luastotal = kepala.getLuas()+jaritangankanan.getLuas()+jaritangankiri.getLuas()
                +jarikakikanan.getLuas()+ jarikakikiri.getLuas()+ tangankananpersegi.getLuas()
                + tangankiripersegi.getLuas()+ kakikanan.getLuas()+ kakikiri.getLuas()+ badan.getLuas();
        double tinggitubuh = kepala.getDiameter()+ badan.panjang+ kakikanan.panjang +
                jarikakikiri.getDiameter();
        System.out.println("Luas total dari tubuh Robot = " + luastotal);
        System.out.println("Tinggi badan Robot = " + tinggitubuh );
    }
}